import { environment } from "src/environments/environment";

export const baseUrl = environment.production ? 'https://ravi-foodbox.cfapps.io' : 'https://ravi-foodbox.cfapps.io/'
export const restaurantUrl = baseUrl + '/restaurant'
export const foodUrl = baseUrl + '/food'
export const userUrl = baseUrl + '/user'
export const cartUrl = baseUrl + '/cart'
export const orderUrl = baseUrl + '/order'